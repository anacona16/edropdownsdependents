<?php

class EDropDownsDependents extends CInputWidget
{
  public $defaultAction = '/site/dropdowns';  // Se configura el acion en siteController.php asi: 'dropdowns' => array('class' => 'ext.EDropDownsDependents.EDropDownsDependentsAction')
  public $dataModel;                          // Modelo de donde se sacaran las opciones para el dropDownList puede ser String nombre del Modelo o array(key => value) Si es un segundo dropDownList dependiente poner un array() vacio
  public $title = 'title';                    // Nombre del campo en la tabla que se usara para mostrar en las opciones del dropDownList
  public $targetModel;                        // Nombre del modelo para consultar las opciones para el segundo dropDownList
  public $targetMethod = false;               // Nombre del metodo que se usara para llenar el dropdownlist, recibe el id como parametro, debe retornar un CHtml::listData
  public $targetField;                        // Nombre del campo que guarda la relacion con la tabla superior
  public $targetTitle = 'title';              // Nombre del campo que se usara para mostrar en las opciones del segundo dropDownList
  public $dropDownListTarget = false;         // Nombre de la propiedad en el modelo actual ej: $form->dropDownList($model, 'codimuni', array()); "codimuni"
  public $showImageCharging = true;           // Booleano muestra o no una imagen de cargando junto al lado derecho del dropDownList
  public $relationName = false;

  private $_assetsFolder;
  private $_dataArrayOptions;

  public function init() 
  {
    parent::init();

    $this->registerCoreScripts();
  }

  public function run() 
  {
    if (is_array($this->dataModel)) {
      $this->_dataArrayOptions = $this->dataModel;
    } else if ($this->targetMethod) {
      $this->_dataArrayOptions = $this->targetModel;
    } else {
      $dataModel = new $this->dataModel;
      $this->_dataArrayOptions = CHtml::listData($dataModel->model()->findAll(), $dataModel->model()->tableSchema->primaryKey, $this->title);
    }

    $_resolveNameID = $this->resolveNameID();

    if ($this->hasModel()) {

      if (!$this->model->isNewRecord && $this->relationName !== false){
        $titleField = $this->title;
        $relationName = $this->relationName;
        $this->_dataArrayOptions = array($this->value => $this->model->$relationName->$titleField);
      }

      self::renderDataFile(CHtml::activeDropDownList($this->model, $this->attribute, $this->_dataArrayOptions, $this->htmlOptions));
      $_htmlID = CHtml::activeId($this->model, $this->attribute);
    } else {
      self::renderDataFile(CHtml::dropDownList($_resolveNameID[0], $_resolveNameID[1], $this->_dataArrayOptions, $this->htmlOptions));
      $_htmlID = $_resolveNameID[1];
    }

    if ($this->dropDownListTarget !== false) {
      // preparamos algunas opciones para pasarselas al objeto javascript llamado DropDowns que crearemos mas abajo.
      $options = CJavaScript::encode(array(
        'firstField' => $_htmlID,
        'action' => CHtml::normalizeUrl($this->defaultAction), // importante
        'targetModel' => $this->targetModel,
        'targetMethod' => $this->targetMethod,
        'targetField' => $this->targetField,
        'targetTitle' => $this->targetTitle,
        'dropDownListTarget' => $this->dropDownListTarget,
        'showImageCharging' => $this->showImageCharging,
        'imageCharging' => CHtml::image($this->_assetsFolder . '/loading.gif', 'Cargando', array('id' => 'image-charging-edropdownlist')),
      ));

      // insertamos el objeto Javascript DropDowns, el cual reside
      // en un archivo JS externo (en los assets).
      // le pasamos las opciones a su constructor con el objeto de 
      // comunicar las dos piezas.
      Yii::app()->getClientScript()->registerScript("dropdowns_corescript_" . $_htmlID, "new DropDowns({$options})");
    }
  }

  public function registerCoreScripts() 
  {
    $localAssetsDir = dirname(__FILE__) . '/assets';
    $this->_assetsFolder = Yii::app()->getAssetManager()->publish($localAssetsDir);

    $cs = Yii::app()->getClientScript();

    foreach (scandir($localAssetsDir) as $f) {
      $_f = strtolower($f);
      if (strstr($_f, ".js"))
        $cs->registerScriptFile($this->_assetsFolder . "/" . $_f);
      if (strstr($_f, ".css"))
        $cs->registerCssFile($this->_assetsFolder . "/" . $_f);
    }
  }

  public function renderDataFile($data)
  {
    CBaseController::renderFile(__DIR__ . '/view.php', array('data' => $data));
  }

  public function runAction($action, $data) 
  {
    if ($action == 'dropdowns') {

      //Creamos instancia del modelo del cual se optendran los datos para el segundo dropDownList
      $model = new $data['targetModel'];
      //Nombre del metodo para obtener el listado de opciones
      $method = $data['targetMethod'];
      //Nombre del campo que guarda la relacion entre las dos tablas master-detail
      $attribute = $data['targetField'];
      //Capturamos el ID necesario para hacer la consutla.
      $id = $data['id'];
      //Nombre del campo que se usara para mostrar en el segundo dropDownList
      $titleRelation = $data['targetTitle'];

      if ($method) {
        if (method_exists($model, $method)) {
          $_listData = $model->$method($id);
        } else {
          //Creo un CHtml::listData es mas facil para luego recorrer, retorna un array con clave valor
          $_listData = CHtml::listData($model->model()->findAllByAttributes(array($attribute => $id)), $model->tableSchema->primaryKey, $titleRelation);
        }
      }
      
      $options = NULL;
      
      //Creo opciones para el segundo dropDownList
      foreach ($_listData as $key => $value) {
        $options .= CHtml::tag('option',array(
                                'value'=>$key,
                              ), CHtml::encode($value),true);
      }

      return self::renderDataFile($options);
    }
  }
}