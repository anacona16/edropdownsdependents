Widget que provee dropDownList dependientes
===========================================

por: Ramiro Anacona Meneses    <anacona16@gmail.com>

Probado en: Yii 1.1.13

EDropDownsDependents es un widget para [Yii Framework](http://www.yiiframework.com "Yii Framework") desarrollado para facilitar el uso de Combos dependientes en formularios.

De una manera mas directa tenemos el siguiente ejemplo: Una relacion Maestro-Detalle entre Paises y Departamentos/Estados, y un Maestro-Detalle entre Departamentos/Estados y Municipios y asi segun se necesite.

En esta imagen se puede ver el model ER entre las tablas

![Master Detail Capture](https://bitbucket.org/anacona16/edropdownsdependents/downloads/master-detail.png "Master Detail Capture")

Como se puede ver la tabla tb_cpoblados (Centro poblados, lo que seria pueblos mas pequeños dentro de municipios --En mi pais, Colombia--) al crear un nuevo registro de donde se requiera el codigo de un Municipio tendriamos que indicar el Departamento/Estado, pero listar todos los Departamentos/Estados en un solo DropDownList no seria lo mejor, por eso se utilizan Combos dependientes, siguiendo el ejemplo, para seleccionar un municipio anteriormente debi seleccionar un Departamento/Estado y para hacer esto ultimo debi antes seleccionar un Pais (El ejemplo lo hice con toda esta jerarquia para mostrar que EDropDownsDependents puede usarse en un mismo formulario varias veces)

Al usar EDropDownsDependents para el ejemplo antes descrito, al final se mirara asi:

![EDropDownsDependents Capture](https://bitbucket.org/anacona16/edropdownsdependents/downloads/screenshotDropDown.png "EDropDownsDependents Capture")

INSTALAR
--------

## 1) Clonar el repositorio GIT

    cd /home/myapp/protected/extensions
    git clone https://anacona16@bitbucket.org/anacona16/edropdownsdependents.git

    Si no usa GIT copie la carpeta completa edropdownsdependents en su carpeta de extensions

### DESCARGA DIRECTA
Por favor descargue el zip, y descomprima en /protected/extensions/edropdownsdependents

## 2) Configure '/protected/config/main.php'

    'import'=>array(
      'application.models.*',
      'application.components.*',
      'ext.edropdownsdependents.*',   // <---
    ),

## 3) Configure la Accion

Conecte el widget con la aplicacion usando un Action en SiteController (o uno diferente si lo prefiere)

Por defecto, se usa:

myapp/protected/controllers/SiteController.php

IMPORTANTE:
  Esta configuracion es requerida solamente una vez para todo el proyecto


~~~
[php]
  public function actions()
  {
    return array(
      'captcha'=>array(
        'class'=>'CCaptchaAction',
        'backColor'=>0xFFFFFF,
      ),
      'page'=>array(
        'class'=>'CViewAction',
      ),
      'dropdowns'=>array(                       // <---
        'class'=>'EDropDownsDependentsAction',  // <---
      ),                                        // <---
    );
  }

~~~

## 4) Insertar y configurar el Widget
 
### Version 0.2 27-09-213)

Esta version incluye cambios significativos en la extension:
      
    Evita declarar las propiedades de las tablas externas en el modelo de la ultima tabla
    2. Si el modelo (fila de la tabla) se esta editando, por defecto pone el valor actual, evita tener que voler a selecionar todos los datos.
      2.1. El cambio mas significativo tiene como requerimiento crear una relacion en el modelo, como se supone que las tablas tiene una relacion esto no deberia causar mayor trabajo.
  
### 4.1) Ejemplo de primer DropDownList sin modelo asociado
~~~
[php]
<?php
  $this->widget('ext.EDropDownsDependents.EDropDownsDependents', array(
    'name' => 'pais', // Cuando la propiedad no esta en la entidad (campo en la tabla)
    'dataModel' => array(170 => 'Colombia'), // Array clave valor con los datos a mostrar en el DropDownList
    'targetModel' => 'Departamentos', //Nombre del modelo para consultar las opciones para el segundo dropDownList
    'targetField' => 'codipais', //Nombre del campo que guarda la relacion con la tabla superior
    'targetTitle' => 'nombdepa', //Nombre del campo que se usara para mostrar en las opciones del segundo dropDownList
    'dropDownListTarget' => 'departamento', //ID Html del DropDownList donde se cargaran los datos
    'htmlOptions' => array(
      'empty' => 'Seleccione Pais'
   ),
));
?>
~~~

### 4.2) Ejemplo de primer DropDownList con modelo asociado
~~~
[php]
<?php
    'dataModel' => 'Paises', // Nombre del modelo
?>
~~~

### 4.2) Ejemplo de segundo DropDownList con modelo asociado
Para este caso al parametro dataModel se pasa un array vacio, pon especial atencion al valor del parametro dropDownListTarget
~~~
[php]
<?php
    'dataModel' => array(), // Nombre del modelo
    'dropDownListTarget' => CHtml::activeID($model, 'codimuni'),  // Si se cambia el atributo ID en el DropDownListTarget a travez del htmlOptions, cambiar aqui!
?>
~~~

### 4.3) Ejemplo de ultimo DropDownList // Este campo pertene a la tabla, por ende cuando se edita no siempre se cambia este valor
NOTA: Si es el ultimo dropDownList no necesitas declarar el parametro dropDownListTarget
~~~
[php]
<?php
    'relationName' => 'pais', // Nombre de la relacion en el modelo
    'title' => 'nombrepais', // Nombre del campo en la tabla que se usara para mostrar en el DropDownList
?>
~~~

## 5) Otras Opciones

~~~
[php]
'htmlOptions' => array(), // ver http://www.yiiframework.com/doc/api/1.1/CHtml#dropDownList-detail
'showImageCharging' => true, // Muestra gif Cargando
'defaultActionName' => '/site/dropdowns', // Si se cambia el Action se debe especificar en el controller
'relationName' => false, // Si este es el ultimo DropDownList es conveniente especificar el nombre de la relacion con la tabla superior, ayuda cuando el modelo se esta editando
'value' => $model->propiedada, // Si este el ultimo DropDownList se debe especificar el value, esto con el fin de: Cuando se edite el registro mostrar el valor actual
~~~